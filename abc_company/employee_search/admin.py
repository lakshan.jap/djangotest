from django.contrib import admin
from employee_search.models import Employee

# Register your models here.
admin.site.register(Employee)
