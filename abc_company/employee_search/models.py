from django import db
from django.db import models
from django.db.models.fields import CharField

# Create your models here.


class Employee(models.Model):
    employee_id = models.CharField(primary_key=True, max_length=10,
                                   null=False, db_column='employee_id')
    full_name = models.CharField(
        max_length=100, null=False, db_column='full_name')
    gender = models.CharField(max_length=100, null=False, db_column='gender')
    date_of_birth = models.CharField(max_length=10, db_column='date_of_birth')
    joined_date = models.CharField(max_length=10, db_column='joined_date')
    salary = models.IntegerField(db_column='salary_(usd)')
    branch = models.CharField(max_length=100, null=False, db_column='branch')

    class Meta:
        db_table = '"employees"'
