from django.urls import path, re_path
from employee_search.views import employee_results


urlpatterns = [
    re_path(r'^details/$', employee_results, name='details')
]
