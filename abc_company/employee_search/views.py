from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from django.db.models.expressions import RawSQL

from employee_search.models import Employee
from employee_search.serializers import EmployeeSerializer
# Create your views here.


@api_view(['GET', ])
def employee_results(request):
    response = ""
    queryparam = request.GET
    first_name = queryparam.get('first_name')
    last_name = queryparam.get('last_name')
    branch = queryparam.get('branch')
    country = queryparam.get('country')

    try:
        if first_name and last_name and country and branch:
            response = Employee.objects.filter(
                full_name=first_name+' '+last_name).filter(branch=branch).filter(branch=country)
        elif first_name and last_name:
            response = Employee.objects.filter(
                full_name=first_name+' '+last_name)
        elif first_name:
            response = Employee.objects.filter(
                full_name__istartswith=first_name)
        elif last_name:
            response = Employee.objects.filter(full_name__endswith=last_name)
        elif branch:
            response = Employee.objects.filter(branch=branch)
        elif country:
            response = Employee.object.get(Branch=country)
    except Employee.DoesNotExist:
        return Response(status.HTTP_404_NOT_FOUND)
    except Exception as exc:
        return Response(status.HTTP_500_INTERNAL_SERVER_ERROR)

    if request.method == "GET":
        data = {}
        serializer = EmployeeSerializer(response, many=True)
        data['data'] = serializer.data
        data['status'] = status.HTTP_200_OK
        data['success'] = 'Data fetching success'

        return Response(data)
