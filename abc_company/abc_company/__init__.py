from os import read
from sqlite3.dbapi2 import Error
import pandas as pd
import sqlite3
import csv
import io

conn = sqlite3.connect('db.sqlite3')


def feed_db():
    s = io.StringIO()
    try:
        with open('employee.csv') as file:
            for line in file:
                s.write(str(line).replace(", ,", ","))
        s.seek(0)

        employees = pd.read_csv(
            s, delimiter=',', sep='delimiter', encoding="utf-8", skipinitialspace=True, skip_blank_lines=True)
        employees.columns = [c.strip().lower().replace(' ', '_')
                             for c in employees.columns]
        employees.to_sql('employees', conn, if_exists='replace', index=False)

    except Error as exc:
        raise Error('Database initialization failed', exc)


if __name__ != '__main__':
    feed_db()
